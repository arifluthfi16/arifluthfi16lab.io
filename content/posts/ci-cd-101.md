---
title: "CI CD an Overview"
date: 2021-09-23T00:12:58+07:00
draft: false
tags : ["CI/CD", "Concepts", "Software Engineering"]
categories : "CI/CD"
---

# Overview

In this article we will be learning about CI/CD. 

- What is it?
- Why you should care?
- How to do it?



# What is CI/CD?

CI CD are two different things, but both of them are still in the scope of software development. CI/CD are software development practices that enables faster development and deployments. Simply CI/CD can provide you with a pipeline that can automate the process of test, build, and deployments. 

## Continuous Integration (CI)

So what is continuous integrations?

> Commit code to a shared repo frequently
>
> *Fireship.io - CI/CD In 100 Seconds*

> Continuous integration (CI) is the practice of automating the integration of code changes from multiple contributors into a single software project. 
>
> *atlassian.com*

From those definition above we could say that Continuous Integration is a process that triggers when a new code  getting pushed to the repo.

When there is a new push to the repo, it will automatically triggers process of build, test, and maybe other quality assessment. Here I found a nice image that explains the process.

![img](../../static/images/ci-cd-101/Picture01.png)

Source : https://nanduribalajee.medium.com/what-is-ci-cd-pipeline-e2f25db99bbe



### What can you add to CI Pipeline?

Here is a list of few things that you can add to your CI Pipeline.

1. Unit Testing
2. Integration Testing
3. Software Quality Assessment
4. Load Testing
5. Performance Testing

If there is a failure in any of the process in the pipeline you will get a notice, you can also setup the notice to be delivered in different platforms : Mail, Slack, Discord, GitHub Notifications, and many more.



## Continuous Delivery  (CD)

From what I gathered Continuous Delivery is just another layer above the Continuous Integration pipeline, where in CI the pipeline will build and test the software, in CD you will have a system that will prepare your software to be deployed anytime by click of a button.

>  Continuous delivery is an extension of continuous integration since it automatically deploys all code changes to a testing and/or production environment after the build stage. 
>
> This means that on top of automated testing, you have an automated release process and you can deploy your application any time by clicking a button. 
>
> *atlassian.com*

So it will be your responsibility to schedule or plan when to release the software.



### Further about Continuous Deployment

We can take Continuous Delivery further to Continuous Deployment. Simply all it does is automate the deployment process, where in Continuous Delivery we still need to plan when to release it in Continuous Deployment everything is handled by the system without any human intervention except when there is a failure on the pipeline process. 



# Why you should care?

Where may it be obvious that creating a CI/CD pipeline will cost an effort. There many benefits come with it

**Automate Everything, Less Bug, Faster Testing**

It is kind of obvious if you have a pipeline that check / test your software every time before it getting released, failure will be captured early and can be handled faster. In example, you can build a pipeline that does an API Integration testing to check if every endpoints can fulfill the required requests, therefore you do not have to worry if there is a failure or an API that does not match with the requirements.

**Less Deploying Complexity**

If you have a Continuous Deployment you don't have to worry about deployment and making sure everything work like it should, since you have the deployment process automated already, you can ensure that it will deliver your product perfectly every time.

**Faster Evaluation & Improvement**

Not only that Continuous Integration can test software, you can also attach a quality assessment to the pipeline so for every push you can evaluate and improve the code immediately without having to wait a separated assessment process to finish.



There are obviously many more benefits if you integrate CI/CD to your SDLC, but 3 points above are the most that I interested in, you can find more references about the benefits in the reference section below.



# How you can do it?

We have been talking about what is it, and why you should care about it. But how do we do it?? how do we create a CI/CD pipeline ? There are a lot of tools out there that you can use to accommodate CI/CD integration to your SDLC. here are few of them : 

1. [GitLab CI/CD](https://docs.gitlab.com/ee/ci/)
2. [GitHub Action](https://github.com/features/actions)
3. Jenkins
4. Circle CI
5. Team City
6. Bamboo
7. Travis CI

Some of them are opensource and free to use, you might wanted to try building a CI/CD pipelines in a small projects and see the impact by yourself before implementing it into a bigger one, because implementing a CI/CD pipelines will require efforts and also adaptation for the team. Yes implementing CI/CD might be a good idea but what if your team is not ready for it? it will just add more technical debt and might cause a bottleneck. So consider carefully! 



That is all! Thanks for reading.

This article is written by me as a note, as a sharing platform, and as a milestone of my learning process! So please feel free to drop me a comments or any critique about it! Thankyou! Have a wonderful day!



# References

1. https://www.atlassian.com/continuous-delivery/continuous-integration
2. https://nanduribalajee.medium.com/what-is-ci-cd-pipeline-e2f25db99bbe
3. https://www.atlassian.com/continuous-delivery/principles/continuous-integration-vs-delivery-vs-deployment
4. https://www.katalon.com/resources-center/blog/ci-cd-tools/
