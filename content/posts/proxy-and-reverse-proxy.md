---
title: "Proxy and Reverse Proxy"
date: 2021-04-11T19:55:32+07:00
draft: false
tags : ["Networking", "Concepts", "Proxy", "Reverse Proxy"]
categories : "Networking"
---

# Proxy

Proxy or also known as Forward Proxy, are server that act as a gateway between you and the internet. Proxy server act as an intermediary server separating the end user and the website they browse. Other than forwarding web request modern proxy server do cool things such as : 

- Access Control
- Balance Traffic
- Caching
- Logging
- Anonymous Access

The most simple example of proxy is when you are trying to access a website but then it says that it is blocked. Usually ISP does this. They usually block porn sites or even political sites. 

## How Proxy Server Works

![image-20210410225048065](../../static/images/proxy-and-reverse-proxy/image-20210410225048065.png)

In a standard internet communication, we usually told that every application / websites have a server. But in the reality we did not directly connect to the server. Usually clients will connect to a proxy server from ISP and then the ISP will forward the request to the target server. This target server might be another proxy server.  

The response from the target server will be redirected back to the clients. Some proxy sever enables caching, this makes a frequent web request faster since the proxy server does not need to request to a server. In the end proxy server are simply an intermediary server that serves between client and the target server.  

## Use Cases

There are a lot of uses cases for proxy servers, here in this article we will talk about a small portion of it.

### Access Control

By using proxy you can control access of clients, example : in a campus environment they usually set up a proxy so that students cannot access sites like facebook, twitter, instagram or other social media when they are using the campus wifi. Other common example is ISP that blocks porn sites. 

### Caching

By doing caching you can improve performance of a network, this done by saving a commonly accessed websites locally. Doing so every time user request for the web page, we don't have to get a new request from the target server. Instead we can use a saved cache. But caching a site like this could be a problem if done incorrectly and can cause a *Stale Cache* Problem.

### Logging

Since every access to the internet need to come across the proxy server, you can record the traffic and use the data as a log.

### Anonymity

When there is no proxy between the target server and the user, user information could be recorded by the target user. But by using proxy the user IP can be hidden and access could be more anonymous.



# Reverse Proxy

Just like a forward proxy server, reverse proxy sits between the client and the target server as an intermediary server. But the main purpose of reverse proxy is different, reverse proxy server will receive client connection and then they will pick which server will serve the request, Reverse Proxy are common used in a server architecture that have a high load. A few cool things that reverse proxy do such as : 

- Load Balancing
- Caching
- Ingress
- Canary Deployment
- Attack Protection
- SSL Encryption

## How Reverse Proxy Works

![image-20210411123415646](../../static/images/proxy-and-reverse-proxy/image-20210411123415646.png)

When a client request to the server, the request will come into the reverse proxy server  this reverse proxy server will be inspect the request and forward the request to a server based on a mechanism or algorithm like round robin where each server will get a request one after another this will make the load are distributed properly.

There a lot of software that can serves a reverse proxy server here is a list of the 'Engines' that can serve reverse proxy.

## Web Server that do Reverse Proxy

![image-20210410194548639](../../static/images/proxy-and-reverse-proxy/image-20210410194548639.png)



1. HAProxy
2. NGINX
3. Istio
4. Envoy
5. Linkerd

There is a lot of choices when you wanted to pick a web server to create a reverse proxy, but the most common one is NGINX and HAProxy



## Use Cases

### Load Balancing

One of the most common use case of reverse proxy is load balancing, reverse proxy can distribute request load to the cluster by using algorithm like round robin or other mechanism. By doing so the request load of a server can be reduced and performance can be greatly improved.

### Caching

You can setup a caching system in a reverse proxy, by doing so every common request to the server can be be served much faster since it does not have to get the request from the server.

### Ingress

Ingress simply route the traffic for a certain request, in example we have 2 server, **Server 1 - Serves Images** &**Server 2 - Serves Data**. if the client wanted to access images the reverse proxy server will route the client to server 1, and if the user wanted to access data it will route to server 2. 

### Canary Deployment

There is an experiment in youtube where 3% of the user will randomly selected and got a video with AI generated thumbnail. How do they achieve this? This They are using method called Canary Deployment. Where a certain server will have a unique modification and they will redirect 3% of the user to that server. 



# Sources

1. https://www.youtube.com/watch?v=SqqrOspasag
2. https://www.cloudflare.com/learning/cdn/glossary/reverse-proxy/
3. https://www.fortinet.com/resources/cyberglossary/proxy-serve



That is all for this article, thankyou for reading the article! This article might lack in a lot of things, I am open for any critics or comments. Hit me up on twitter!