---
title: "Uber H3 Geospatial Indexing System Overview"
date: 2022-01-13T16:02:32+07:00
draft: false
category : ["data"]
tags : ["data", "geospatial", "h3"]
---



# What is Geospatial Data?

Geospatial data is information that describes any objects, event, or other feature with a location on or near the surface of the earth. It usually combines location information (coordinates like lat & long) and the information attributes it can be population, poverty, sales per region, etc. In short: Geospatial data is information recorded in conjunction with a geographic indicator of some type.

Usually you would find Geospatial data in a form of Vector, Raster, and Coordinate System. a more practical example would be a a [Geo Fence Polygon](https://en.wikipedia.org/wiki/Geo-fence) that created using multiple Coordinates of Lat & Long. Inside the Polygon there could be any kind of information like a Zone, Numbers of Visitor, Number of Objects or anything.

This Polygon can solves a lot of problems, Uber also uses this technique before they are using H3. But the problem with this technique is that it does not scale well. Uber used this Geo Fencing to mark which area is busy and have a high demand they create the polygon manually by hand this will cause a lot of issue when you try to scale it, imagine if you need to scale this globally, then you will need mark everything manually. 

You may already can see why Uber moved on from this solution already. But do note that there are a lot of reasons why they moved from this solution, you could check the references below to find out more why.

# What is H3 Geospatial Index

> H3 is an open source framework developed by Uber in the C Programming Language. At its core, H3 is a geospatial analysis tool that provides a hexagonal, hierarchical spatial index to gain insights from large geospatial datasets. **The building blocks of H3 are different sized regular hexagonal polygons.** These polygons are spread out over the entire projection of the earth map from pole to pole. This means that any location on the planet can be attributed to a H3 Hexagon down to a precision of 0.0000009 km² area.
>
> From : [Uber H3 For Data Analysis](https://towardsdatascience.com/uber-h3-for-data-analysis-with-python-1e54acdcc908)

From the definition above we know that H3 is already covered the entire globe so you don't have to worry about creating Hexagons manually. In H3 to find a Hexagon or a place you will need it's index Example : 8a283082a677fff. You could also specify a location using [Placekey] (https://h3geo.org/docs/comparisons/placekey/). In this example above i am using [Unfolded](https://studio.unfolded.ai/map/5d1303e5-6d1c-4c33-acaa-b998e7ee87c9) San Francisco Elevation example dataset. 

![image-20220113152109919](../../static/images/uber-h3-overview/image-20220113152109919.png)



Now you can find a location in H3 using H3 Indexes or Place Key, but how about sizes? What if you need a bigger or smaller hexagons? H3 Already provided few number of Resolution you can pick varying on your use case:

![img](../../static/images/uber-h3-overview/1*2pK-bAH_y45Y7pZ7vgEJtQ.png)

Most of the time you won't switch resolution on the fly.



# H3 Use cases

As you can imagine there is a lot of thing you can do with H3, by using an existing library you can simply convert Lat & Long into an H3 Indexes from there we can work with any Geo Coordinates easily and interchangeably. Here is a few use cases i found:

1. You can use H3 to visualize demand

2. You can use H3 to visualize information easier

3. By using H3 Polyfill you can create essentially a geofence based on H3.

   <img src="../../static/images/uber-h3-overview/image-20220113154139550.png" alt="image-20220113154139550" style="zoom:50%;" />

   Do keep in mind that the Geo Fence created will not be 100% accurate from the polygon you described, since H3 is based on Hexagons there might be an overshot area or a small area that is not covered by the Hexagons.

4. Visualizing a Migration Pattern of Snow Owl, or literally any moving objects.

   This is a really cool way to use H3, you can check it more on [Here](https://www.youtube.com/watch?v=1Ocw_oaw_a8)

5. Visualize distribution of objects, this is one quite similar with visualizing demand. You can create a highlighted area when the index reached certain amount of value thresholds.



That's it! 

From this super short artcile i hope you get a grasp what is H3 and what can you do about it, in next article we will explore H3 using python in a more Hands on Way! 

As usual, thanks for reading and have a great day :). 



# Source

1. Main Source: https://h3geo.org/docs/
2. Reason for Uber H3: https://www.youtube.com/watch?v=ay2uwtRO3QE
3. More on H3: 
   1. https://www.youtube.com/watch?v=1Ocw_oaw_a8
   2. https://towardsdatascience.com/uber-h3-for-data-analysis-with-python-1e54acdcc908
4. How to H3: https://foursquare.com/article/how-to-use-h3-for-geospatial-analytics/
5. Uber Engineering: https://eng.uber.com/h3/
6. About Geospatial Data: 
   1. https://www.ibm.com/id-en/topics/geospatial-data
   2. https://www.safe.com/what-is/spatial-data/



