---
title: "Measuring Code Readability"
date: 2021-03-19T15:10:42+07:00
draft: true
showtoc : true
tocopen: false
---

# Code Readability?

Readability defined as a human judgment of how easy a text is to understand. The readability of a program is related to its maintainability, and is thus a key factor in overall software quality [2].

Code readability and documentation readability is a critical factor that contribute to overall software maintainability. Few research noted that act of reading code is one of the most time consuming activities in a maintenance activities. It is also suggested to check source code in a project for readability to ensure maintainability, portability and reusability of the code.



## Why Readability Matter?

Do you wanted to create an elegant, maintainable and standardized code? then you should care more about your code Readability. 



## Code Readability and Software Quality

There is a strong relation between Code Readability and Software Quality, a good code readability indicates that the software has a good maintainability which led to higher Software Quality. 



# How to Measure Readability?

To measure code Readability there is a handful selection of method that can be used such as [1] : 

**Measuring Method : **

1. Flesch's Reading Ease Score *
2. Dale-Chall's Readability Formula
3. SPACHE Readability Formula
4. FryGraph Readability Formula
5. SMOG Grading
6. Cloze Procedure
7. Lively-Pressey's Formula and Gunning's Fog Index (FOG)



# How to Create Readable Code

There is a lot of factor that come in mind when creating a readable code, and most of them will depend on your personal / team taste.  But here is a few General guideline when you wanted to create a more readable code : 

1. Practice Clean Code
2. Use Notations
3. Follow Solid Principle



# References

1. https://www.ijert.org/research/an-accurate-model-of-software-code-readability-IJERTV1IS6390.pdf
2. https://web.eecs.umich.edu/~weimerw/p/weimer-tse2010-readability-preprint.pdf