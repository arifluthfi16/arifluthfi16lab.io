---
title: "How to Host Database on Google Cloud SQL"
date: 2021-11-10T05:40:22+07:00
draft: false
tags : ["cloud", "google cloud", "mysql", "storage", "cloud sql"]
category : ["Google Cloud"]
---

# Overview

In this article we will talk about how we can create a database in google cloud Cloud SQL.

> Cloud SQL is **a fully-managed database service that helps you set up, maintain, manage, and administer your relational databases on Google** Cloud Platform. You can use Cloud SQL with MySQL, PostgreSQL, or SQL Server. 
>
> from : cloud.google.com/sql



## **Prerequisite**

1. Google Cloud Account

2. Database Tool

   I am using [DBeaver](https://dbeaver.io/)



## **Project Goals**

1. Create a new Google Cloud SQL Instance from the Cloud Console
2. Access the newly created Google Cloud SQL from our local machine]
3. Create and run existing SQL Scripts



# Creating new Cloud SQL Instance

Login to your google cloud, and navigate to your console. Select an existing project or create a new one. 

1. In the search box type 'sql' then you should see **SQL** in the section **Product & Pages**

![image-20211110203440850](../../static/images/how-to-host-database-on-google-cloud-sql/image-20211110203440850-16365634602971.png)

2. Click On **Create Instance**

![image-20211110203702204](../../static/images/how-to-host-database-on-google-cloud-sql/image-20211110203702204-16365634602982.png)

3. Cloud SQL provided several database engine, here we will be using MySQL.	

![image-20211110204307081](../../static/images/how-to-host-database-on-google-cloud-sql/image-20211110204307081-16365634602993.png)

4. Configure the instance

![image-20211110204943166](../../static/images/how-to-host-database-on-google-cloud-sql/image-20211110204943166-16365634602994.png)

Here is some **Configuration Details**:

**Instance ID**

> The instance ID is the name of the instance. It **is used to uniquely identify your instance within the project**. Choose an instance name that is aligned with the purpose of the instance when possible.

You can fill Instance ID with anything you wanted, it is just a way to identify the Cloud SQL instance. 

**Password**

You could fillin the password manually, or you could just generate the password. Make sure you save your password in a note somewhere.

**Database Version**

We will be using MySQL 8.0 since it is the latest one and it also support older version of SQL.

**Region**

Note that Region cannot be changed after the instance of created, while you can change your zone any time. If you wanted to you could pick a region that suite your purposes. Picking a region will be super important when you wanted to use the Cloud SQL for production. But for current scenario I will just stick to the default.

![image-20211110211750731](../../static/images/how-to-host-database-on-google-cloud-sql/image-20211110211750731-16365634603005.png)

**Zone Availability**

You could select Multiple Availability Zones to ensure that the service will have High Availability, but it comes with a cost. Since our current purpose is just to create an instance and try it out we will pick **Single Zone**.

![image-20211110212558237](../../static/images/how-to-host-database-on-google-cloud-sql/image-20211110212558237-16365634603006.png)

After you finish fiddling with the configuration click **Create Instance**. 

![image-20211110213236858](../../static/images/how-to-host-database-on-google-cloud-sql/image-20211110213236858-16365634603007.png)

It will take a few minutes to create the instance. After it completed you have finished creating your Cloud SQL Instance.



# Connecting to the Instance

Open your MySQL Client or Database Explorer, i am using DBeaver it is a free and universal database tools check it out [here](https://dbeaver.io/) 

1. Configure Authorized Networks, 

   In the Side Navigation go to **Connections**, you can see that there is a warning that we have not authorized any external network. To add one simply click on **Add Network**.

   ![image-20211110232015318](../../static/images/how-to-host-database-on-google-cloud-sql/image-20211110232015318-16365634603018.png)

   Then Add a new network with this setting:

   ![image-20211110231829128](../../static/images/how-to-host-database-on-google-cloud-sql/image-20211110231829128-16365634603019.png)

   This setting will allow anyone to connect to the database, please note that this is not a recommended approach when you wanted to make a production database. But for this tutorial this one is enough, click save after you finished editing the new configuration.

2. Create a new connection and select MySQL

   ![image-20211110214546282](../../static/images/how-to-host-database-on-google-cloud-sql/image-20211110214546282-163656346030110.png)

   When adding a new connection, you will be prompted to enter the connection info, please note that we are connecting to the database using HTTP. To check your connection information head to recently created instance page and look for **Connect to the Instance**. We will use the public ip as our Host.

![image-20211110214822185](../../static/images/how-to-host-database-on-google-cloud-sql/image-20211110214822185-163656346030111.png)

3. Enter the connection information.

   Use the public IP Address as the host, for the available user you could check it **Users** in the side navigation bar

   ![image-20211110215352182](../../static/images/how-to-host-database-on-google-cloud-sql/image-20211110215352182-163656346030112.png)

   Usually a newly created instance will have default user as root (may varies on the database you used) with the password that we generate earlier. Enter the network configuration and test the connection.

   ![image-20211110232514589](../../static/images/how-to-host-database-on-google-cloud-sql/image-20211110232514589-163656346030113.png)

   If the connection created successfully you should see this.

   ![image-20211110232622090](../../static/images/how-to-host-database-on-google-cloud-sql/image-20211110232622090-163656346030114.png)

​	Click finish, and you should be able to browse the connected database.

![image-20211110232725207](../../static/images/how-to-host-database-on-google-cloud-sql/image-20211110232725207-163656346030215.png)

​	From this point your database is ready to use.



# Create a Database 

1. From your DBeaver menu open a new console

![image-20211110233035379](../../static/images/how-to-host-database-on-google-cloud-sql/image-20211110233035379-163656346030216.png)



2. In the console type

   ```sql
   CREATE DATABASE sample_db;
   ```

   To execute query block the script and use `CTRL + Enter` (Select the script one by one)

3. You can clarify that a new database is created from the Cloud SQL Interface.

   ![image-20211110233406963](../../static/images/how-to-host-database-on-google-cloud-sql/image-20211110233406963-163656346030217.png)



4. Creating a Table & Permissions

```sql
USE sample_db;

CREATE TABLE Person (
    PersonID int,
    LastName varchar(255),
    FirstName varchar(255),
    Address varchar(255),
    City varchar(255)
);
```

If you tried to execute this you might encounter this error:

```sql
SQL Error [1142] [42000]: CREATE command denied to user 'root'@'180.244.xxx.xxx' for table 'Person'
```

This happens because by default even the root user did not granted all permission at once, read it [here](https://cloud.google.com/sql/docs/mysql/error-messages), to give the user certain permission you can use :

``` sql
USE mysql;

GRANT CREATE TABLESPACE ON *.* TO 'root' WITH GRANT OPTION;
```

Now if you re run the SQL Script to create table, it will work with no error.

![image-20211110235238523](../../static/images/how-to-host-database-on-google-cloud-sql/image-20211110235238523-163656346030218.png)

That's It! As always thank you for reading this article, and feel free to ask any question or give critique! Thank you!



# References

1. Creating & Connecting to Cloud SQL Instance : 

   https://download.huihoo.com/google/gdgdevkit/DVD1/developers.google.com/cloud-sql/docs/before_you_begin.html#create

2. Network Authorization : 

   https://cloud.google.com/sql/docs/mysql/authorize-networks

3. Authorizing All Network:

   https://stackoverflow.com/questions/28339849/google-cloud-sql-authorize-all-ips

4. SQL Grant Statement

   - https://chartio.com/resources/tutorials/how-to-grant-all-privileges-on-a-database-in-mysql/
   - https://www.ibm.com/docs/en/qmf/12.2.0?topic=privileges-sql-grant-statement
   - https://cloud.google.com/sql/docs/mysql/error-messages

