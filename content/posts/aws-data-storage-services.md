---
title: "AWS Data Storage Services"
date: 2021-07-07T23:49:13+07:00
draft: false
tags: ["aws", "cloud", "database", "object storage"]
category: ["cloud"]
---

# Introduction

Recently I have been digging into AWS, for a beginner like me many types of data store in AWS quite confused me. In this article I will try to simply explain what I know so far. few topics that will be discussed in this article such as : 

1. Amazon Elastic Block Store (Amazon EBS)
2. Amazon Simple Storage Service (Amazon S3)
3. Amazon Elastic File System (Amazon EFS)
4. Amazon Relational Database service (Amazon RDS)
5. Amazon Dynamo DB



# Amazon Elastic Block Store (EBS)

When using EC2 instance, often we will need to access to a block level storage (hard disk) just like a normal machine would do, EC2 provided multiple ways for us to communicate with a block level storage.



## Instance Store

Instance store is a temporary block level storage for Amazon EC2 instance, when you launch a new EC2 instance usually it already came with a default hard disk volume, that is what instance store is, you could do read and write data operation with this storage but when you stop the EC2 instance the data stored will be deleted.

That is why it is not recommended to put crucial data in an instance store, often instance store are used for caching or temporary file. 

[More on instance store](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/InstanceStorage.html)

## About EBS

To save data in a persistent way we could use EBS, just like instance store EBS is also a block level storage that could be used with amazon EC2 instance. EBS creates a virtual volume that can freely attach and detach from an EC2 instance. EBS does not bind to a certain machine, since it is virtual by nature we could easily move it around. You can even attach multiple EBS to an EC2.

 Now when you stop your EC2 instance, every data that are saved on the EBS volume will be stored persistently, you could create an incremental backup with EBS by creating Amazon EBS snapshot. The first time snapshot created it will copy all the data in the volume, the second time we take another snapshot it will only copy the new part of the volume and will reference the rest to older snapshot. 

![         Snapshots capturing an initial volume state and two subsequent states after data has           been changed.       ](../../static/images/aws-data-storage-services/snapshot_1a.png)



Read more about how incremental snapshot work in EBS Here :  [How incremental snapshot works on EBS](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/EBSSnapshots.html)



# Amazon Simple Storage Service (S3)

Amazon S3 store data on a object-level storage what this mean is that rather than treated as a block of memory, data that stored in s3 are treated as an individual object. If you ever use service like google drive you will found a lot of similarity on this service. Every object in amazon S3 will be stored in a bucket, you could think a bucket as a folder that groups object. 

Since amazon S3 does not require an EC2 instance to operate we could classify this service as a serverless service, and unlike a block-level storage you don't need to choose a specific volume size for your storage, you can scale your S3 storage indefinitely. 

In amazon S3 you could create a specific config to only allow access to specific users, also for every object in amazon S3 you could have a specific URL to access it. It is really flexible and easy to monitor amazon S3 since you can directly access the data in the web rather than EBS storage that you need to run an EC2 instance first.

There are few types of amazon S3 :

1. S3 Standard
2. S3 Standard-Infrequent Access (S3 Standard-IA)
3. S3 One Zone-Infrequent Access (S3 One Zone-IA)
4. S3 Intelligent-Tiering
5. S3 Glacier
6. S3 Glacier Deep Archive

By using amazon S3 you don't have to worry to pick the right volume for the instance, you could just use it as you need it. Amazon S3 are really ideal for storing files and picture for consumer use. But when you need to process a big chunk of data; let's say that you need to crunch a video data with the size of 80gb you might wanted to consider using amazon EBS. 



# Amazon Elastic File System (EFS)

Amazon EFS is a file storage service where multiple clients like app, server can access data that stored in folder at the same time. Amazon EFS use block storage model and also utilize a local file system for file management. Client can access file from the file path.

Since it used block storage model to store data, it look similar to amazon EBS but one main difference between EBS and EFS is that, EFS has the ability to scale automatically so  it is really recommended for system that often scale up or down.



# Amazon Relational Database Service (RDS)

Amazon RDS is a service that enables you to run a relational database on the AWS cloud. currently amazon RDS supports 6 types of database :

1. Amazon Aurora
2. PostgreSQL
3. MySQL
4. MariaDB
5. Oracle Database
6. Microsoft SQL Server

If you have an on-premise server that runs one of system above, you could easily move your system to AWS Cloud, you could use database migration service; you can move entire on-premise system to AWS Cloud with less or even no extra configuration required. 



# Amazon DynamoDB

Amazon DynamoDB is a NoSQL database that use Key-Value approach. Amazon DynamoDB is a serverless database, so there is no need for you to configure an instance or other infrastructure. Even Amazon DynamoDB have an automatic scaling system. Amazon DynamoDB also stores data into several availability zone so it ensure data will be highly available and have a fast response time.



# Amazon Redshift

Amazon Redshift is a Data Warehousing service that can be used for big data analytics, this service offers you ability to collect data from multiple source and also collect trends from all the data. 

Amazon Redshift Spectrum enables you to execute a single SQL query for exabytes of unstructured data in the data lake. 

Not only can handle a big chunk of data set, amazon redshift also handles it gracefully it can process and deliver data faster, so it will reduce business intelligence workloads. 



# Other Databases Service

1. **Amazon DocumentDB**

   Amazon DocumentDB is a NoSQL database that use Document oriented approach to store the data, it works closely just like mongodb. 

2. **Amazon Neputune**

   Amazon Neptune is a graph database services.

3. **Amazon Managed Blockchain**

   Amazon Blockchain enables data exchange without the need to trust.

4. **Amazon Quantum Ledger Database (QLDB)**

   QLDB is a immutable database system where an entry cannot be removed or edited once it is on the system, this service provide centralized transaction log that cannot be changed and also verified by cryptography.

5. **Amazon ElastiCache**

   This service enables you to add a caching layer to database so it can increase database read time, ElastiCache can use 2 from of data store which is : Redis & Memcached.

   

   

    

